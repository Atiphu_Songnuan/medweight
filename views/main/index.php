<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>MedWeight</title>
    
    <link rel="icon" type="image/png" href="./public/images/icons/dumbbell.ico"/> 

    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="./public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <!-- SB Admin 2 -->
    <link href="./public/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
        body {
            font-family: 'Kanit', serif;
            font-size: 28px;
        }
        label{
            font-size: 18px;
        }
        h4{
            font-size: 24px;
        }
        h3{
            font-size: 28px;
        }
        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }
        div.dataTables_info , a{
            font-size: 14px;
        }
        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            vertical-align: middle;
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        td{
            white-space:nowrap;
            font-size: 14px;
            /* vertical-align: middle; */
        }
        tr{
            font-family: 'Kanit', serif;
            font-size: 16px;
        }

        .edit-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
        }
        .edit-modal .modal {
            background: transparent !important;
        }
        .swal2-popup {
		font-size: 0.7rem !important;
	    }
    </style>
</head>
<body class="wrapper">

    <!-- Require Header from header.php -->
    <?php require './views/header.php'?>

    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h4 class="mb-0 text-gray-800">ค้นหาบุคลากร</h4>
        </div>

        <div class="row">
            <!-- Card 1 -->
            <div class="col-xl-12 col-md-12 mb-12">
                <div class="form-group">
                    <input class="form-control" type="number" placeholder="รหัสบุคลากร" id="inputperid">
                    <button class="btn btn-primary form-control" onclick="SearchPersonal()" id="btnsearch">ค้นหา</button>
                    <!-- <button class="btn btn-primary form-control" onclick="SearchPersonal()">ค้นหา</button> -->
                </div>
            </div>

            <div class="col-xl-12 col-md-12 mb-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="personweightdatatable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>รหัสพนักงาน</th>
                                <th>ชื่อ-นามสกุล</th>
                                <th>หน่วยงาน</th>
                                <th>น้ำหนักก่อน (kg)</th>
                                <th>น้ำหนักหลัง (kg)</th>
                                <th>สี</th>
                                <th>รุ่นการแข่งขัน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $personweightJsonDecode = json_decode($this->PersonWeightData,true);
                                for ($i=0; $i < count($personweightJsonDecode); $i++) { 
                                    $personid = $personweightJsonDecode[$i]['PERID'];
                                    echo '<tr>';
                                        echo '<td align="center">';
                                            if ($personweightJsonDecode[$i]['ID'] != "") {
                                                echo $personweightJsonDecode[$i]['ID'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td class="text-success" align="center" style="cursor: pointer;" onclick="UpdateWeight('.$personid.')">';
                                            if ($personweightJsonDecode[$i]['PERID'] != "") {
                                                echo $personweightJsonDecode[$i]['PERID'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td class="text-info" style="cursor: pointer;" onclick="UpdateWeight('.$personid.')">';
                                            if ($personweightJsonDecode[$i]['NAME'] != "" && $personweightJsonDecode[$i]['SURNAME'] != "") {
                                                echo $personweightJsonDecode[$i]['NAME']. " " . $personweightJsonDecode[$i]['SURNAME'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td>';
                                            if ($personweightJsonDecode[$i]['DEPARTMENT'] != "") {
                                                echo $personweightJsonDecode[$i]['DEPARTMENT'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td align="center">';
                                            echo '<h6>';
                                                echo '<span class="badge badge-danger">';
                                                    if ($personweightJsonDecode[$i]['WEIGHT_BEFORE'] != "") {
                                                        echo $personweightJsonDecode[$i]['WEIGHT_BEFORE'];
                                                    } else{
                                                        echo "-";
                                                    }
                                                echo '</span>';
                                            echo '</h6>';
                                        echo '</td>';

                                        echo '<td align="center">';
                                            echo '<h6>';
                                                echo '<span class="badge badge-success">';
                                                    if ($personweightJsonDecode[$i]['WEIGHT_AFTER'] != "") {
                                                        echo $personweightJsonDecode[$i]['WEIGHT_AFTER'];
                                                    } else{
                                                        echo "-";
                                                    }
                                                echo '</span>';
                                            echo '</h6>';
                                        echo '</td>';


                                        $color = "";
                                        switch ($personweightJsonDecode[$i]['COLOR']) {
                                            case 'ฟ้า':
                                                # code...
                                                $color = "#95DEE3";
                                                break;
                                            case 'เหลือง':
                                                # code...
                                                $color = "#F6D155";
                                                break;
                                            case 'ชมพู':
                                                # code...
                                                $color = "#E8B5CE";
                                                break;
                                            case 'ม่วง':
                                                # code...
                                                $color = "#6B5B95";
                                                break;
                                            default:
                                                # code...
                                                break;
                                        }
                                        echo '<td align="center" style="background-color:'.$color.'; color:white">';
                                            if ($personweightJsonDecode[$i]['COLOR'] != "") {
                                                echo $personweightJsonDecode[$i]['COLOR'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td align="center">';
                                            if ($personweightJsonDecode[$i]['TYPE'] != "") {
                                                echo $personweightJsonDecode[$i]['TYPE'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End Card 1 -->
        </div>
    </div>

    <div class="modal fade" id="personal-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="col-12 modal-title">ข้อมูลบุคลากร</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center">
                        <div class="col-md-12">
                        <!-- 41701 -->
                            <img id="personimg" alt="person" class='rounded-circle' width="150px" height="150px" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label><b>รหัสบุคลากร: </b></label>
                            <label id="peridlabel"></label>
                        </div>

                        <div class="col-md-12">
                            <label><b>หน่วยงาน: </b></label>
                            <label id="departlabel"></label>
                        </div>
                        <div class="col-md-6">
                            <label><b>ชื่อ-นามสกุล: </b></label>
                            <label id=namelabel></label>&nbsp<label id="surnamelabel"></label>
                        </div>

                        <div class="col-md-12">
                            <label><b>น้ำหนักก่อน (kg)</b></label>
                            <input class="form-control" type="number" id="weightbefore" min="0" max="99999" maxlength="3">
                        </div>

                        <div class="col-md-12">
                            <label><b>น้ำหนักหลัง (kg)</b></label>
                            <input class="form-control" type="number" id="weightafter" min="0" max="99999" maxlength="3" disabled>
                        </div>

                        <div class="col-md-12">
                            <label><b>สี</b></label>
                            <select class="form-control" id="color">
                                <option value="0">เลือกสี</option>
                                <option value="1">ฟ้า</option>
                                <option value="2">เหลือง</option>
                                <option value="3">ชมพู</option>
                                <option value="4">ม่วง</option>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <label><b>รุ่น</b></label>
                            <select class="form-control" id="type">
                                <option value="0">เลือกรุ่น</option>
                                <option value="1">อายุน้อยกว่า 30 ปี</option>
                                <option value="2">อายุระหว่าง 31-40 ปี</option>
                                <option value="3">อายุระหว่าง 41-50 ปี</option>
                                <option value="4">อายุมากกว่า 50 ปี</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    <button type="button" id="btnconfirm" class="btn btn-primary"></button>
                </div>
            </div>
        </div>
    </div>

   <!-- Bootstrap core JavaScript-->
   <script src="./public/vendor/jquery/jquery.min.js"></script>
    <script src="./public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Datatable -->
    <script src="./public/vendor/datatables/jquery.dataTables.js"></script>
    <script src="./public/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Demo scripts for this page-->
    <script src="./public/js/demo/datatables-demo.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="./public/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="./public/js/sb-admin-2.min.js"></script>

    <!-- SweetAlert2 -->
    <script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

    <script>
        $(function(){
            $('#personweightdatatable').dataTable({
                "order": [[ 0, "desc" ]],
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                        "searchable": false
                    }
                ]
            });
        });

        // Press Enter to Login
        $('body').keyup(function(e) {
            // console.log('keyup called');
            var code = e.keyCode || e.which;
            if (code == '13') {
                SearchPersonal();
                $('#btnsearch').focus();
            }
        });

        function SearchPersonal(){
            var jsondata = {"perid": $('#inputperid').val()};
            // console.log(jsondata);
            $('#weightbefore').val('');
            $('#weightbefore').attr('disabled', false);
            $('#weightafter').attr('disabled', true);
            $('#weightafter').val('');
            $('#color').val(0);
            $('#color').attr('disabled', false);
            $('#type').val(0);
            $('#type').attr('disabled', false);

            $.ajax({
                type:"POST",
                url:"../medweight/ApiService/CheckDuplicatePersonByID",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function(response) {
                    // console.log(response);
                    if (response.length == 0) {
                        $.ajax({
                            type:"POST",
                            url:"../medweight/ApiService/GetPersonDataByID",
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: JSON.stringify(jsondata),
                            contentType: "application/json",
                            dataType: "json",
                            success: function(response) {
                                // console.log(response);
                                if (response.length != 0) {
                                    $('#personal-modal').modal('show');
                                    for (let index = 0; index < response.length; index++) {
                                        const dataElement = response[index];
                                        $('#personimg').attr("src", "<?php echo PERSONALPICTURE;?>" + dataElement['PERID']);
                                        $('#personimg').attr("onerror", "this.onerror=null;this.src='./public/images/man.svg';" );
                                        $('#peridlabel').text(dataElement['PERID']);      
                                        $('#departlabel').text(dataElement['Dep_name']);
                                        $('#namelabel').text(dataElement['NAME']);      
                                        $('#surnamelabel').text(dataElement['SURNAME']);      
                                        // console.log(dataElement['PERID']);     
                                        $('#btnconfirm').text('บันทึก');
                                        $('#btnconfirm').attr('onclick', 'PersonWeightConfirm()');                 
                                    }
                                } else{
                                    Swal.fire({
                                        title: 'ไม่พบข้อมูลบุคลากร',
                                        text: '',
                                        type: 'error',
                                        timer: 2000,
                                        showConfirmButton: false,
                                    })
                                }
                            },
                            error: function(response){
                                console.log(response.responseText);
                            }
                        });
                    } else{
                        Swal.fire({
                            title: 'ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมีข้อมูลของคุณ '+ response[0]['NAME'] + " อยู่แล้ว",
                            text: '',
                            type: 'error',
                            timer: 3000,
                            showConfirmButton: false,
                        })
                    }
                },
                error: function(response){
                    console.lPersonWeightConfirmog(response.responseText);
                }
            }); 
        }

        function PersonWeightConfirm(){
            // console.log('อัพเดทน้ำหนักก่อนแข่ง');
            var jsondata = {
                "perid":$('#peridlabel').text(),
                "name":$('#namelabel').text(),
                "surname":$('#surnamelabel').text(),
                "department":$('#departlabel').text(),
                "weight_before": $('#weightbefore').val(),
                "color": $('#color option:selected').text(),
                "type": $('#type option:selected').text()
            }

            $.ajax({
                type:"POST",
                url:"../medweight/ApiService/InsertPersonWeight",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function() {
                    Swal.fire({
                        title: 'เพิ่มข้อมูลสำเร็จ',
                        text: '',
                        type: 'success',
                        timer: 2000,
                        showConfirmButton: false,
                        onClose: () =>{
                            window.location.href='../medweight/main';
                        }
                    })
                },
                error: function(response) {
                    console.log(response.responseText);
                }
            });
        }

        function UpdateWeight(personid){
            // console.log(personid);
            $('#personal-modal').modal();

            var jsondata = {'perid': personid};
            $.ajax({
                type:"POST",
                url:"../medweight/ApiService/GetPersonWeightDataByID",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function(response) {
                    // console.log(response);
                    $('#personimg').attr("src", "<?php echo PERSONALPICTURE;?>" + response[0]['PERID']);
                    $('#personimg').attr("onerror", "this.onerror=null;this.src='./public/images/man.svg';" );

                    $('#peridlabel').text(response[0]['PERID']);      
                    $('#departlabel').text(response[0]['DEPARTMENT']);
                    $('#namelabel').text(response[0]['NAME']);      
                    $('#surnamelabel').text(response[0]['SURNAME']);

                    $('#weightbefore').val(response[0]['WEIGHT_BEFORE']);
                    $('#weightbefore').attr('disabled', true);

                    $('#weightafter').attr('disabled', false);

                    var coloroptionvalue = 0;
                    switch (response[0]['COLOR']) {
                        case 'ฟ้า':
                            coloroptionvalue = 1;
                            break;
                        case 'เหลือง':
                            coloroptionvalue = 2;
                            break;
                        case 'ชมพู':
                            coloroptionvalue = 3;
                            break;
                        case 'ม่วง':
                            coloroptionvalue = 4;
                            break;
                        default:
                            break;
                    }
                    $('#color').val(coloroptionvalue);
                    $('#color').attr('disabled', true);

                    var typeoptionvalue = 0;
                    switch (response[0]['COLOR']) {
                        case 'ฟ้า':
                            typeoptionvalue = 1;
                            break;
                        case 'เหลือง':
                            typeoptionvalue = 2;
                            break;
                        case 'ชมพู':
                            typeoptionvalue = 3;
                            break;
                        case 'ม่วง':
                            typeoptionvalue = 4;
                            break;
                        default:
                            break;
                    }
                    $('#type').val(typeoptionvalue);
                    $('#type').attr('disabled', true);

                    $('#btnconfirm').text('แก้ไข');
                    $('#btnconfirm').attr('onclick', 'PersonWeightAfterConfirm()');

                },
                error: function(response){
                    console.log(response);
                }
                
            });
        }

        function PersonWeightAfterConfirm(){
            // console.log('อัพเดทน้ำหนักหลังแข่ง');
            if ($('#weightafter').val() != '') {
                var jsondata = {
                    "perid":$('#peridlabel').text(),
                    "weight_after": $('#weightafter').val(),
                };

                $.ajax({
                    type:"POST",
                    url:"../medweight/ApiService/UpdateWeightAfter",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: JSON.stringify(jsondata),
                    contentType: "application/json",
                    dataType: "json",
                    success: function(response) {
                        if (response) {
                            Swal.fire({
                                title: 'อัพเดทข้อมูลสำเร็จ',
                                text: '',
                                type: 'success',
                                timer: 2000,
                                showConfirmButton: false,
                                onClose: () =>{
                                    window.location.href='../medweight/main';
                                }
                            })
                        }
                    },
                    error: function(response){
                        console.log(response);
                    } 
                });          
            } else{
                Swal.fire({
                    title: 'กรุณาใส่น้ำหนักให้ถูกต้อง',
                    text: '',
                    type: 'warning',
                    timer: 2000,
                    showConfirmButton: false
                });
            }
             
        }
    </script>
</body>
</html>