<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function CheckDuplicatePersonByID($perid)
    {
        $sql = 'SELECT PERID, NAME FROM medpersonweight WHERE PERID = ' . $perid . '';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    // For Modal
    public function GetPersonDataByID($perid)
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = 'SELECT PERID, NAME, SURNAME, Dep_name FROM viewpersonindepart WHERE PERID = ' . $perid . '';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }
    // *************************************

    public function InsertPersonWeight($persondata)
    {
        // echo $persondata->name;
        if (isset($persondata)) {
            $data = [
                'perid' => $persondata->perid,
                'name' => $persondata->name,
                'surname' => $persondata->surname,
                'department' => $persondata->department,
                'weight_before' => $persondata->weight_before,
                'color' => $persondata->color,
                'type' => $persondata->type,
            ];

            $sql = 'INSERT INTO  medpersonweight(PERID, NAME, SURNAME, DEPARTMENT, WEIGHT_BEFORE, COLOR, TYPE, CHECK_IN_DATE)
                    VALUES  (:perid, :name, :surname, :department, :weight_before, :color, :type, NOW())';

            $sth = $this->db->prepare($sql);
            $sth->execute($data);

            echo true;
        } else {
            echo false;
        }
    }

    // For Datatable
    public function GetPersonWeightData()
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = 'SELECT ID, PERID, NAME, SURNAME, DEPARTMENT, WEIGHT_BEFORE, WEIGHT_AFTER, COLOR, TYPE, CHECK_IN_DATE FROM medpersonweight ORDER BY CHECK_IN_DATE DESC';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        return $jsonData;
    }

    public function GetPersonWeightDataByID($perid)
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = 'SELECT PERID, NAME, SURNAME, DEPARTMENT, WEIGHT_BEFORE, COLOR, TYPE FROM medpersonweight WHERE PERID = ' . $perid . '';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    public function UpdateWeightAfter($perid, $weightafter)
    {
        if (isset($perid) && isset($weightafter)) {
            $data = [
                'perid' => $perid,
                'weightafter' => $weightafter
            ];

            $sql = 'UPDATE  medpersonweight
                    SET     WEIGHT_AFTER=:weightafter
                    WHERE   PERID = :perid';

            $sth = $this->db->prepare($sql);
            $sth->execute($data);

            echo true;
        } else{
            echo false;
        }
    }
}
