<?php
class Main extends Controller
{
    public function __construct()
    {
        parent::__construct('main');
        $this->views->PersonWeightData = $this->model->GetPersonWeightData();
    }
    

    public function index()
    {
        $this->views->render('main/index');
    }

}
